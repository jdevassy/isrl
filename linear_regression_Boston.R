# Linear Regression on the Boston data set from the MASS library in R

#variables
# lstat = percentage of households with low socioeconomic status
# medv = median value of house

# First load data
library('MASS')
library('ISLR')
library(car) #for vif() function import

#Let's look at the predictors available in the dataset
names(Boston)

#let's fit a model to lstat and medv
lm.fit <- lm(Boston$medv~Boston$lstat)
print('Modelling a linear regression between lstat and medv')
print(lm.fit)
names(lm.fit)

#Obtain a confidence interval
cint <- confint(lm.fit)
print('Confidence interval')
print(cint)

#predict for new data points
print('new lstat data 5, 10, 15')
predict(lm.fit, data.frame(Boston$lstat=c(5, 10, 15))), interval='prediction')


#plots
par(mfrow=c(2,2))
plot(Boston$lstat, Boston$medv)
abline(lm.fit, lwd=5, col='red')
plot(predict(lm.fit), residuals(lm.fit))
plot(predict(lm.fit), rstudent(lm.fit))

#Leverage statistics
plot(hatvalues(lm.fit))
print('observation with largest leverage statistic')
print(which.max(hatvalues(lm.fit)))

#build a multiple regression model with all the available predictors
lm.fit <- lm(Boston$medv~., data=Boston)
summary(lm.fit)
#R-squared statistic
summary(lm.fit)$r.sq
#RSE statistic
summary(lm.fit)$sigma
#variance inflation between predictors? Compute VIF
vif(lm.fit)

#remove the 'age' predictor from the model
lm.fit <- update(lm.fit, ~.-Boston$age)
summary(lm.fit)
summary(lm.fit)$r.sq
summary(lm.fit)$sigma

#interaction between predictors lstat and black
summary(lm(Boston$medv~Boston$age*Boston$lstat, data=Boston))

#polynomial regression for medv and lstat
lm.fit_2 <- lm(Boston$medv~Boston$lstat+I(Boston$lstat ^ 2)) #quadratic factor for lstat
lm.fit <- lm(Boston$medv~Boston$lstat) #linear regression
summary(lm.fit)
plot(lm.fit)
#compare quadratic, linear models by ANOVA
anova(lm.fit, lm.fit_2)
par(mfrow=c(2,2))
plot(lm.fit_2)


